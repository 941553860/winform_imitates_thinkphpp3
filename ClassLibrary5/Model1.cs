namespace ClassLibrary5
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    

    //[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public partial class Model1 : DbContext
    {

        public Model1()
            : base("name=Model1")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ValidateOnSaveEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<app_version> app_version { get; set; }
        public virtual DbSet<attribute> attribute { get; set; }
        public virtual DbSet<bank_account> bank_account { get; set; }
        public virtual DbSet<batch_record_history> batch_record_history { get; set; }
        public virtual DbSet<car> car { get; set; }
        public virtual DbSet<car_detail> car_detail { get; set; }
        public virtual DbSet<company> company { get; set; }
        public virtual DbSet<company_warehouse> company_warehouse { get; set; }
        public virtual DbSet<currency> currency { get; set; }
        public virtual DbSet<customs_store> customs_store { get; set; }
        public virtual DbSet<dealer_shop> dealer_shop { get; set; }
        public virtual DbSet<debit> debit { get; set; }
        public virtual DbSet<debit_status> debit_status { get; set; }
        public virtual DbSet<debit_type> debit_type { get; set; }
        public virtual DbSet<department> department { get; set; }
        public virtual DbSet<employee> employee { get; set; }
        public virtual DbSet<file_info> file_info { get; set; }
        public virtual DbSet<finance> finance { get; set; }
        public virtual DbSet<finance_detail> finance_detail { get; set; }
        public virtual DbSet<import_log> import_log { get; set; }
        public virtual DbSet<logistics> logistics { get; set; }
        public virtual DbSet<mft_status> mft_status { get; set; }
        public virtual DbSet<order_item> order_item { get; set; }
        public virtual DbSet<order_item_detail> order_item_detail { get; set; }
        public virtual DbSet<payment> payment { get; set; }
        public virtual DbSet<payment_method> payment_method { get; set; }
        public virtual DbSet<payment_status> payment_status { get; set; }
        public virtual DbSet<payment_type> payment_type { get; set; }
        public virtual DbSet<platform> platform { get; set; }
        public virtual DbSet<produce_plan> produce_plan { get; set; }
        public virtual DbSet<produce_plan_detail> produce_plan_detail { get; set; }
        public virtual DbSet<produce_plan_material> produce_plan_material { get; set; }
        public virtual DbSet<produce_plan_material_detail> produce_plan_material_detail { get; set; }
        public virtual DbSet<product> product { get; set; }
        public virtual DbSet<product_bom> product_bom { get; set; }
        public virtual DbSet<product_brand> product_brand { get; set; }
        public virtual DbSet<product_catalog> product_catalog { get; set; }
        public virtual DbSet<product_detail> product_detail { get; set; }
        public virtual DbSet<product_diy> product_diy { get; set; }
        public virtual DbSet<product_level> product_level { get; set; }
        public virtual DbSet<product_number> product_number { get; set; }
        public virtual DbSet<product_number_business_type> product_number_business_type { get; set; }
        public virtual DbSet<product_number_fee> product_number_fee { get; set; }
        public virtual DbSet<product_number_gj_status> product_number_gj_status { get; set; }
        public virtual DbSet<product_number_hg_status> product_number_hg_status { get; set; }
        public virtual DbSet<product_number_status> product_number_status { get; set; }
        public virtual DbSet<product_origin> product_origin { get; set; }
        public virtual DbSet<product_rank> product_rank { get; set; }
        public virtual DbSet<product_type> product_type { get; set; }
        public virtual DbSet<province> province { get; set; }
        public virtual DbSet<purchase> purchase { get; set; }
        public virtual DbSet<purchase_detail> purchase_detail { get; set; }
        public virtual DbSet<purchase_instore> purchase_instore { get; set; }
        public virtual DbSet<purchase_instore_detail> purchase_instore_detail { get; set; }
        public virtual DbSet<purchase_payment_status> purchase_payment_status { get; set; }
        public virtual DbSet<quality> quality { get; set; }
        public virtual DbSet<quality_detail> quality_detail { get; set; }
        public virtual DbSet<quality_detail_report_image> quality_detail_report_image { get; set; }
        public virtual DbSet<quality_detail_temporary> quality_detail_temporary { get; set; }
        public virtual DbSet<quality_result> quality_result { get; set; }
        public virtual DbSet<quality_temporary> quality_temporary { get; set; }
        public virtual DbSet<recipe> recipe { get; set; }
        public virtual DbSet<recipe_detail> recipe_detail { get; set; }
        public virtual DbSet<record_history> record_history { get; set; }
        public virtual DbSet<role> role { get; set; }
        public virtual DbSet<role_group> role_group { get; set; }
        public virtual DbSet<role_user> role_user { get; set; }
        public virtual DbSet<sale_customer> sale_customer { get; set; }
        public virtual DbSet<sale_order> sale_order { get; set; }
        public virtual DbSet<sale_order_detail> sale_order_detail { get; set; }
        public virtual DbSet<sale_order_diy> sale_order_diy { get; set; }
        public virtual DbSet<sale_order_outstore> sale_order_outstore { get; set; }
        public virtual DbSet<sale_order_outstore_detail> sale_order_outstore_detail { get; set; }
        public virtual DbSet<sale_order_promotion> sale_order_promotion { get; set; }
        public virtual DbSet<sale_order_status> sale_order_status { get; set; }
        public virtual DbSet<sale_order_target_detail> sale_order_target_detail { get; set; }
        public virtual DbSet<sale_order_xml> sale_order_xml { get; set; }
        public virtual DbSet<sale_refund> sale_refund { get; set; }
        public virtual DbSet<sale_refund_detail> sale_refund_detail { get; set; }
        public virtual DbSet<storage_basket> storage_basket { get; set; }
        public virtual DbSet<supplier> supplier { get; set; }
        public virtual DbSet<sync_log> sync_log { get; set; }
        public virtual DbSet<sync_type> sync_type { get; set; }
        public virtual DbSet<trader> trader { get; set; }
        public virtual DbSet<trader_type> trader_type { get; set; }
        public virtual DbSet<transform_item> transform_item { get; set; }
        public virtual DbSet<transform_item_detail> transform_item_detail { get; set; }
        public virtual DbSet<transform_order> transform_order { get; set; }
        public virtual DbSet<transform_order_close> transform_order_close { get; set; }
        public virtual DbSet<transform_order_detail> transform_order_detail { get; set; }
        public virtual DbSet<transform_order_history> transform_order_history { get; set; }
        public virtual DbSet<transform_order_info> transform_order_info { get; set; }
        public virtual DbSet<transform_order_promotion> transform_order_promotion { get; set; }
        public virtual DbSet<transform_order_status> transform_order_status { get; set; }
        public virtual DbSet<transform_order_target_detail> transform_order_target_detail { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<user_group> user_group { get; set; }
        public virtual DbSet<user_group_detail> user_group_detail { get; set; }
        public virtual DbSet<user_group_role> user_group_role { get; set; }
        public virtual DbSet<user_group_trader> user_group_trader { get; set; }
        public virtual DbSet<user_group_warehouse> user_group_warehouse { get; set; }
        public virtual DbSet<user_type> user_type { get; set; }
        public virtual DbSet<warehouse> warehouse { get; set; }
        public virtual DbSet<warehouse_area> warehouse_area { get; set; }
        public virtual DbSet<warehouse_change> warehouse_change { get; set; }
        public virtual DbSet<warehouse_change_detail> warehouse_change_detail { get; set; }
        public virtual DbSet<warehouse_column> warehouse_column { get; set; }
        public virtual DbSet<warehouse_type> warehouse_type { get; set; }
        public virtual DbSet<whole_sale_order> whole_sale_order { get; set; }
        public virtual DbSet<whole_sale_order_debit_status> whole_sale_order_debit_status { get; set; }
        public virtual DbSet<whole_sale_order_detail> whole_sale_order_detail { get; set; }
        public virtual DbSet<whole_sale_order_status> whole_sale_order_status { get; set; }
        public virtual DbSet<whole_sale_outstore> whole_sale_outstore { get; set; }
        public virtual DbSet<whole_sale_outstore_detail> whole_sale_outstore_detail { get; set; }
        public virtual DbSet<whole_sale_outstore_status> whole_sale_outstore_status { get; set; }
        public virtual DbSet<whole_sale_product_price> whole_sale_product_price { get; set; }
        public virtual DbSet<wms_check_store> wms_check_store { get; set; }
        public virtual DbSet<wms_check_store_detail> wms_check_store_detail { get; set; }
        public virtual DbSet<wms_instore> wms_instore { get; set; }
        public virtual DbSet<wms_instore_detail> wms_instore_detail { get; set; }
        public virtual DbSet<wms_instore_detail_split> wms_instore_detail_split { get; set; }
        public virtual DbSet<wms_instore_status> wms_instore_status { get; set; }
        public virtual DbSet<wms_outstore> wms_outstore { get; set; }
        public virtual DbSet<wms_outstore_detail> wms_outstore_detail { get; set; }
        public virtual DbSet<wms_outstore_detail_pick> wms_outstore_detail_pick { get; set; }
        public virtual DbSet<wms_outstore_detail_split> wms_outstore_detail_split { get; set; }
        public virtual DbSet<wms_store> wms_store { get; set; }
        public virtual DbSet<yinshang> yinshang { get; set; }
        public virtual DbSet<yy_product> yy_product { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<app_version>()
                .Property(e => e.app_version_id)
                .IsUnicode(false);

            modelBuilder.Entity<app_version>()
                .Property(e => e.app_version1)
                .IsUnicode(false);

            modelBuilder.Entity<attribute>()
                .Property(e => e.attribute_id)
                .IsUnicode(false);

            modelBuilder.Entity<attribute>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<attribute>()
                .Property(e => e.attribute_key)
                .IsUnicode(false);

            modelBuilder.Entity<attribute>()
                .Property(e => e.attribute_value)
                .IsUnicode(false);

            modelBuilder.Entity<bank_account>()
                .Property(e => e.bank_account_id)
                .IsUnicode(false);

            modelBuilder.Entity<bank_account>()
                .Property(e => e.bank_account_name)
                .IsUnicode(false);

            modelBuilder.Entity<bank_account>()
                .Property(e => e.bank_account_info)
                .IsUnicode(false);

            modelBuilder.Entity<bank_account>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<bank_account>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.batch_record_history_id)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.user_id)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.source_product_name)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.source_product_code)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.source_product_zh_name)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.target_product_name)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.target_product_zh_name)
                .IsUnicode(false);

            modelBuilder.Entity<batch_record_history>()
                .Property(e => e.target_product_code)
                .IsUnicode(false);

            modelBuilder.Entity<car>()
                .Property(e => e.car_id)
                .IsUnicode(false);

            modelBuilder.Entity<car>()
                .Property(e => e.wms_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<car>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<car_detail>()
                .Property(e => e.car_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<car_detail>()
                .Property(e => e.car_id)
                .IsUnicode(false);

            modelBuilder.Entity<car_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<car_detail>()
                .Property(e => e.batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.company_name)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.company_warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.access_name)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.access_code)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.user_code)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.user_password)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.private_key)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.token)
                .IsUnicode(false);

            modelBuilder.Entity<company_warehouse>()
                .Property(e => e.access_url)
                .IsUnicode(false);

            modelBuilder.Entity<currency>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<currency>()
                .Property(e => e.currency_name)
                .IsUnicode(false);

            modelBuilder.Entity<currency>()
                .Property(e => e.currency_eng_name)
                .IsUnicode(false);

            modelBuilder.Entity<currency>()
                .Property(e => e.sign)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.dealer_shop_id)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.dealer_shop_code)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.dealer_shop_name)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.platform_id)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.app_id)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.secret_key)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.session_id)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.ydz_id)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.taobao_user_nick)
                .IsUnicode(false);

            modelBuilder.Entity<dealer_shop>()
                .Property(e => e.token)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.debit_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.debit_code)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.bank_account_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.debit_type)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.debit_status)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.confirm_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit_status>()
                .Property(e => e.debit_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit_status>()
                .Property(e => e.debit_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<debit_type>()
                .Property(e => e.debit_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<debit_type>()
                .Property(e => e.debit_type_value)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.department_id)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.department_code)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.department_name)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.employee_id)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.department_id)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.employee_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.mobile_phone)
                .IsUnicode(false);

            modelBuilder.Entity<file_info>()
                .Property(e => e.file_info_id)
                .IsUnicode(false);

            modelBuilder.Entity<file_info>()
                .Property(e => e.file_name)
                .IsUnicode(false);

            modelBuilder.Entity<file_info>()
                .Property(e => e.thumb_file_path)
                .IsUnicode(false);

            modelBuilder.Entity<file_info>()
                .Property(e => e.full_file_path)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.finance_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.source_code)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.source_detail_code)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance_detail>()
                .Property(e => e.finance_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance_detail>()
                .Property(e => e.finance_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance_detail>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance_detail>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance_detail>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.import_log_id)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.import_log_name)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.import_type)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.import_log_info)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.file_path)
                .IsUnicode(false);

            modelBuilder.Entity<import_log>()
                .Property(e => e.import_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<logistics>()
                .Property(e => e.logistics_id)
                .IsUnicode(false);

            modelBuilder.Entity<logistics>()
                .Property(e => e.logistics_code)
                .IsUnicode(false);

            modelBuilder.Entity<logistics>()
                .Property(e => e.logistics_name)
                .IsUnicode(false);

            modelBuilder.Entity<logistics>()
                .Property(e => e.logistics_beibei_code)
                .IsUnicode(false);

            modelBuilder.Entity<mft_status>()
                .Property(e => e.mft_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<mft_status>()
                .Property(e => e.mft_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<order_item>()
                .Property(e => e.order_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<order_item>()
                .Property(e => e.order_item_code)
                .IsUnicode(false);

            modelBuilder.Entity<order_item>()
                .Property(e => e.order_item_name)
                .IsUnicode(false);

            modelBuilder.Entity<order_item>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<order_item_detail>()
                .Property(e => e.order_item_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<order_item_detail>()
                .Property(e => e.order_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<order_item_detail>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.payment_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.payment_code)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.bank_account_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.payment_type)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.payment_status)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.confirm_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.target_account_info)
                .IsUnicode(false);

            modelBuilder.Entity<payment>()
                .Property(e => e.target_currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_method>()
                .Property(e => e.payment_method_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_method>()
                .Property(e => e.payment_method_code)
                .IsUnicode(false);

            modelBuilder.Entity<payment_method>()
                .Property(e => e.payment_method_name)
                .IsUnicode(false);

            modelBuilder.Entity<payment_method>()
                .Property(e => e.payment_method_pdd_code)
                .IsUnicode(false);

            modelBuilder.Entity<payment_status>()
                .Property(e => e.payment_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_status>()
                .Property(e => e.payment_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<payment_type>()
                .Property(e => e.payment_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_type>()
                .Property(e => e.payment_type_value)
                .IsUnicode(false);

            modelBuilder.Entity<platform>()
                .Property(e => e.platform_id)
                .IsUnicode(false);

            modelBuilder.Entity<platform>()
                .Property(e => e.platform_code)
                .IsUnicode(false);

            modelBuilder.Entity<platform>()
                .Property(e => e.platform_name)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.produce_plan_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.produce_plan_code)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.target_storage_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.start_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_detail>()
                .Property(e => e.produce_plan_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_detail>()
                .Property(e => e.produce_plan_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_detail>()
                .Property(e => e.target_storage_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.produce_plan_material_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.produce_plan_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.wms_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.produce_plan_material_code)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material_detail>()
                .Property(e => e.produce_plan_material_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material_detail>()
                .Property(e => e.produce_plan_material_id)
                .IsUnicode(false);

            modelBuilder.Entity<produce_plan_material_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_code)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_bar_code)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_name)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_zh_name)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_info)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_remark)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.private_code)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.product_index)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.package_mode)
                .IsUnicode(false);

            modelBuilder.Entity<product>()
                .Property(e => e.unit_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_bom>()
                .Property(e => e.product_bom_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_bom>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_bom>()
                .Property(e => e.product_bom_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_bom>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_brand>()
                .Property(e => e.product_brand_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_brand>()
                .Property(e => e.product_brand_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_brand>()
                .Property(e => e.product_brand_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_brand>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_catalog>()
                .Property(e => e.product_catalog_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_catalog>()
                .Property(e => e.product_catalog_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_catalog>()
                .Property(e => e.product_catalog_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_catalog>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_detail>()
                .Property(e => e.product_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_detail>()
                .Property(e => e.product_detail_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_detail>()
                .Property(e => e.product_detail_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_detail>()
                .Property(e => e.meterial_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_diy>()
                .Property(e => e.product_diy_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_diy>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_diy>()
                .Property(e => e.product_col_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_diy>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_diy>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_diy>()
                .Property(e => e.defult_value)
                .IsUnicode(false);

            modelBuilder.Entity<product_level>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_level>()
                .Property(e => e.product_level_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_level>()
                .Property(e => e.product_level_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_level>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_level>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_number_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_number_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_number_eng_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_number_status)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.hs_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_main_picture)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_chn_mark_picture)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_attach1)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_attach2)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_attach3)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_attach4)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_attach5)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.tariff_number)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.hg_status)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.gj_status)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_property)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_unit)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_supplier)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_brand_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_origin_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number>()
                .Property(e => e.product_bar_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_business_type>()
                .Property(e => e.product_number_business_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_business_type>()
                .Property(e => e.product_number_business_type_value)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_fee>()
                .Property(e => e.product_number_fee_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_fee>()
                .Property(e => e.product_number_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_fee>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_fee>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_gj_status>()
                .Property(e => e.product_number_gj_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_gj_status>()
                .Property(e => e.product_number_gj_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_hg_status>()
                .Property(e => e.product_number_hg_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_hg_status>()
                .Property(e => e.product_number_hg_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_status>()
                .Property(e => e.product_number_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_number_status>()
                .Property(e => e.product_number_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<product_origin>()
                .Property(e => e.product_origin_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_origin>()
                .Property(e => e.product_origin_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_origin>()
                .Property(e => e.product_origin_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_origin>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_rank>()
                .Property(e => e.product_rank_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_rank>()
                .Property(e => e.product_rank_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_rank>()
                .Property(e => e.product_rank_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_rank>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_rank>()
                .Property(e => e.product_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_rank>()
                .Property(e => e.product_brand_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_rank>()
                .Property(e => e.product_origin_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_type>()
                .Property(e => e.product_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_type>()
                .Property(e => e.product_type_code)
                .IsUnicode(false);

            modelBuilder.Entity<product_type>()
                .Property(e => e.product_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<product_type>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<product_type>()
                .Property(e => e.product_catalog_id)
                .IsUnicode(false);

            modelBuilder.Entity<province>()
                .Property(e => e.province_name)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.purchase_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.purchase_code)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.supplier_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.confirm_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_detail>()
                .Property(e => e.purchase_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_detail>()
                .Property(e => e.purchase_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_detail>()
                .Property(e => e.product_index)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_detail>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.purchase_instore_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.purchase_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.purchase_instore_code)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.wms_instore_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore_detail>()
                .Property(e => e.purchase_instore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore_detail>()
                .Property(e => e.purchase_instore_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore_detail>()
                .Property(e => e.purchase_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore_detail>()
                .Property(e => e.product_level_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore_detail>()
                .Property(e => e.product_level_name)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_instore_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_payment_status>()
                .Property(e => e.purchase_payment_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<purchase_payment_status>()
                .Property(e => e.purchase_payment_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<quality>()
                .Property(e => e.quality_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality>()
                .Property(e => e.quality_code)
                .IsUnicode(false);

            modelBuilder.Entity<quality>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.quality_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.quality_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.wms_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.product_level_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail>()
                .Property(e => e.quality_result_image_url)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_report_image>()
                .Property(e => e.report_image_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_report_image>()
                .Property(e => e.quality_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_report_image>()
                .Property(e => e.report_image_url)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.quality_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.quality_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.wms_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.product_level_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_detail_temporary>()
                .Property(e => e.quality_result_image_url)
                .IsUnicode(false);

            modelBuilder.Entity<quality_result>()
                .Property(e => e.quality_result_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_result>()
                .Property(e => e.quality_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_result>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_result>()
                .Property(e => e.source_code)
                .IsUnicode(false);

            modelBuilder.Entity<quality_result>()
                .Property(e => e.product_diy_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_result>()
                .Property(e => e.quality_result1)
                .IsUnicode(false);

            modelBuilder.Entity<quality_result>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<quality_temporary>()
                .Property(e => e.quality_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_temporary>()
                .Property(e => e.quality_code)
                .IsUnicode(false);

            modelBuilder.Entity<quality_temporary>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_temporary>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_temporary>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_temporary>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<quality_temporary>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<recipe>()
                .Property(e => e.recipe_id)
                .IsUnicode(false);

            modelBuilder.Entity<recipe>()
                .Property(e => e.recipe_code)
                .IsUnicode(false);

            modelBuilder.Entity<recipe>()
                .Property(e => e.recipe_name)
                .IsUnicode(false);

            modelBuilder.Entity<recipe_detail>()
                .Property(e => e.recipe_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<recipe_detail>()
                .Property(e => e.recipe_id)
                .IsUnicode(false);

            modelBuilder.Entity<recipe_detail>()
                .Property(e => e.meterial_code)
                .IsUnicode(false);

            modelBuilder.Entity<recipe_detail>()
                .Property(e => e.meterial_name)
                .IsUnicode(false);

            modelBuilder.Entity<record_history>()
                .Property(e => e.record_history_id)
                .IsUnicode(false);

            modelBuilder.Entity<record_history>()
                .Property(e => e.user_id)
                .IsUnicode(false);

            modelBuilder.Entity<record_history>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<record_history>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .Property(e => e.role_id)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .Property(e => e.role_code)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .Property(e => e.role_name)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .Property(e => e.role_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<role_group>()
                .Property(e => e.role_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<role_group>()
                .Property(e => e.role_group_code)
                .IsUnicode(false);

            modelBuilder.Entity<role_group>()
                .Property(e => e.role_group_name)
                .IsUnicode(false);

            modelBuilder.Entity<role_user>()
                .Property(e => e.role_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<role_user>()
                .Property(e => e.role_id)
                .IsUnicode(false);

            modelBuilder.Entity<role_user>()
                .Property(e => e.user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_customer>()
                .Property(e => e.sale_customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_customer>()
                .Property(e => e.sale_customer_code)
                .IsUnicode(false);

            modelBuilder.Entity<sale_customer>()
                .Property(e => e.sale_customer_name)
                .IsUnicode(false);

            modelBuilder.Entity<sale_customer>()
                .Property(e => e.contact_name)
                .IsUnicode(false);

            modelBuilder.Entity<sale_customer>()
                .Property(e => e.contact_phone)
                .IsUnicode(false);

            modelBuilder.Entity<sale_customer>()
                .Property(e => e.mobile_phone)
                .IsUnicode(false);

            modelBuilder.Entity<sale_customer>()
                .Property(e => e.contact_address)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.sale_customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.sale_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.has_invoice)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.invoice_name)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_detail>()
                .Property(e => e.sale_order_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_detail>()
                .Property(e => e.sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_detail>()
                .Property(e => e.product_index)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_diy>()
                .Property(e => e.sale_order_diy_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_diy>()
                .Property(e => e.sale_order_outstore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_diy>()
                .Property(e => e.diy_product_name)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.sale_order_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.sale_order_outstore_code)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore>()
                .Property(e => e.wms_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore_detail>()
                .Property(e => e.sale_order_outstore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore_detail>()
                .Property(e => e.sale_order_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore_detail>()
                .Property(e => e.sale_order_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_outstore_detail>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_promotion>()
                .Property(e => e.sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_promotion>()
                .Property(e => e.promotion_remark)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_status>()
                .Property(e => e.sale_order_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_status>()
                .Property(e => e.sale_order_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_target_detail>()
                .Property(e => e.sale_order_target_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_target_detail>()
                .Property(e => e.sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_target_detail>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_order_xml>()
                .Property(e => e.sale_order_xml1)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.sale_refund_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.sale_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.sale_refund_code)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.platform_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.platform_name)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.dealer_shop_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.dealer_shop_name)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.sale_refund_status)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.refund_reason)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.refund_remark)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.logistics_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund>()
                .Property(e => e.logistics_number)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund_detail>()
                .Property(e => e.sale_refund_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund_detail>()
                .Property(e => e.sale_refund_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund_detail>()
                .Property(e => e.sale_product_id)
                .IsUnicode(false);

            modelBuilder.Entity<sale_refund_detail>()
                .Property(e => e.sale_goods_name)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.storage_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.storage_rack_level_id)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.storage_basket_code)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.storage_basket_name)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.storage_basket_type)
                .IsUnicode(false);

            modelBuilder.Entity<storage_basket>()
                .Property(e => e.position_code)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_id)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_code)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.supplier_name)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.contact_name)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.contact_phone)
                .IsUnicode(false);

            modelBuilder.Entity<supplier>()
                .Property(e => e.contact_address)
                .IsUnicode(false);

            modelBuilder.Entity<sync_log>()
                .Property(e => e.sync_content)
                .IsUnicode(false);

            modelBuilder.Entity<sync_type>()
                .Property(e => e.sync_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<sync_type>()
                .Property(e => e.sync_frequence)
                .IsUnicode(false);

            modelBuilder.Entity<trader>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<trader>()
                .Property(e => e.trader_code)
                .IsUnicode(false);

            modelBuilder.Entity<trader>()
                .Property(e => e.trader_name)
                .IsUnicode(false);

            modelBuilder.Entity<trader>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<trader>()
                .Property(e => e.trader_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<trader_type>()
                .Property(e => e.trader_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<trader_type>()
                .Property(e => e.trader_type_code)
                .IsUnicode(false);

            modelBuilder.Entity<trader_type>()
                .Property(e => e.trader_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item>()
                .Property(e => e.transform_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item>()
                .Property(e => e.transform_item_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item>()
                .Property(e => e.transform_item_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item>()
                .Property(e => e.dealer_shop_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item_detail>()
                .Property(e => e.transform_item_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item_detail>()
                .Property(e => e.transform_item_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item_detail>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item_detail>()
                .Property(e => e.product_number_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item_detail>()
                .Property(e => e.item_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_item_detail>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.transform_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.transform_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.sale_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.mft_number)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.platform_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.platform_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.dealer_shop_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.dealer_shop_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.oto_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.buyer_account)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.payment_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.payment_order_seq)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.payment_method_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.payment_method_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.id_card)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.buyer_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.logistics_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.logistics_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.logistics_number)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.consignee)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.province_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.province_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.district)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.consignee_addr)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.consignee_tel)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.zip_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.goods_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.deliver_status)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.mft_status)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.mft_result)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.check_flag)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.check_msg)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.transform_status)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.transform_status_msg)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.distribution_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.source_platform_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.source_platform_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.source_dealer_shop_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.source_dealer_shop_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.detail_info)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order>()
                .Property(e => e.company_warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.transform_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.sale_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.mft_number)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.platform_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.dealer_shop_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.oto_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.buyer_account)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.payment_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.payment_order_seq)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.payment_method_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.id_card)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.buyer_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.logistics_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.logistics_number)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.consignee)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.province_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.district)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.consignee_addr)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.consignee_tel)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.zip_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.goods_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.mft_status)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.mft_result)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.check_flag)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.check_msg)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.transform_status_msg)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.distribution_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.source_platform_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.source_dealer_shop_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_close>()
                .Property(e => e.detail_info)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.transform_order_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.transform_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.product_number_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.target_product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.target_product_number_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.goods_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_detail>()
                .Property(e => e.unit)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_history>()
                .Property(e => e.user_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_history>()
                .Property(e => e.transform_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_history>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_history>()
                .Property(e => e.sale_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_info>()
                .Property(e => e.transform_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_info>()
                .Property(e => e.transform_order_info_detail)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_info>()
                .Property(e => e.transform_order_info_result)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_promotion>()
                .Property(e => e.promotion_remark)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_status>()
                .Property(e => e.transform_order_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_status>()
                .Property(e => e.transform_order_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.transform_order_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.transform_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.product_number_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.target_product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.target_product_number_code)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.goods_name)
                .IsUnicode(false);

            modelBuilder.Entity<transform_order_target_detail>()
                .Property(e => e.unit)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.user_id)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.user_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.employee_id)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.user_code)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.user_name)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.user_password)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.private_key)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.dealer_shop_id)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.department_id)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.user_phone)
                .IsUnicode(false);

            modelBuilder.Entity<user_group>()
                .Property(e => e.user_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group>()
                .Property(e => e.user_group_code)
                .IsUnicode(false);

            modelBuilder.Entity<user_group>()
                .Property(e => e.user_group_name)
                .IsUnicode(false);

            modelBuilder.Entity<user_group>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_detail>()
                .Property(e => e.user_group_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_detail>()
                .Property(e => e.user_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_detail>()
                .Property(e => e.user_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_detail>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_role>()
                .Property(e => e.user_group_role_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_role>()
                .Property(e => e.user_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_role>()
                .Property(e => e.role_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_role>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_trader>()
                .Property(e => e.user_group_trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_trader>()
                .Property(e => e.user_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_trader>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_trader>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_warehouse>()
                .Property(e => e.user_group_warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_warehouse>()
                .Property(e => e.user_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_group_warehouse>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_type>()
                .Property(e => e.user_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<user_type>()
                .Property(e => e.user_type_code)
                .IsUnicode(false);

            modelBuilder.Entity<user_type>()
                .Property(e => e.user_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse>()
                .Property(e => e.warehouse_code)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse>()
                .Property(e => e.warehouse_name)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse>()
                .Property(e => e.warehouse_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_area>()
                .Property(e => e.warehouse_area_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_area>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_area>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_area>()
                .Property(e => e.warehouse_area_code)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_area>()
                .Property(e => e.warehouse_area_name)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change>()
                .Property(e => e.warehouse_change_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change>()
                .Property(e => e.warehouse_change_code)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.warehouse_change_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.warehouse_change_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.wms_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.storage_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.store_batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_change_detail>()
                .Property(e => e.target_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_column>()
                .Property(e => e.warehouse_column_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_column>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_column>()
                .Property(e => e.warehouse_area_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_column>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_column>()
                .Property(e => e.warehouse_column_code)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_column>()
                .Property(e => e.warehouse_column_name)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_type>()
                .Property(e => e.warehouse_type_id)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_type>()
                .Property(e => e.warehouse_type_code)
                .IsUnicode(false);

            modelBuilder.Entity<warehouse_type>()
                .Property(e => e.warehouse_type_name)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.whole_sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.whole_sale_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.receiver_name)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.receiver_phone)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.receiver_address)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.deliver_name)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.deliver_phone)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.deliver_address)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.contact_number)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.invoice_name)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.confirm_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.whole_sale_order_status)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order>()
                .Property(e => e.whole_sale_order_debit_status)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_debit_status>()
                .Property(e => e.whole_sale_order_debit_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_debit_status>()
                .Property(e => e.whole_sale_order_debit_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_detail>()
                .Property(e => e.whole_sale_order_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_detail>()
                .Property(e => e.whole_sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_detail>()
                .Property(e => e.contact_product_name)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_detail>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_status>()
                .Property(e => e.whole_sale_order_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_order_status>()
                .Property(e => e.whole_sale_order_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.whole_sale_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.whole_sale_order_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.whole_sale_outstore_code)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.whole_sale_outstore_status)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.logistics_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.logistics_number)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.wms_outstore_code)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_detail>()
                .Property(e => e.whole_sale_outstore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_detail>()
                .Property(e => e.whole_sale_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_detail>()
                .Property(e => e.whole_sale_order_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_detail>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_status>()
                .Property(e => e.whole_sale_outstore_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_outstore_status>()
                .Property(e => e.whole_sale_outstore_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_product_price>()
                .Property(e => e.whole_sale_product_price_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_product_price>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_product_price>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<whole_sale_product_price>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.check_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.check_store_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.check_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.confirm_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store_detail>()
                .Property(e => e.check_store_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store_detail>()
                .Property(e => e.check_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store_detail>()
                .Property(e => e.wms_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store_detail>()
                .Property(e => e.store_batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store_detail>()
                .Property(e => e.position_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_check_store_detail>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.wms_instore_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.wms_instore_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.source_instore_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.confirm_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.source_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore>()
                .Property(e => e.trade_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.wms_instore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.wms_instore_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.product_level_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.target_storage_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail>()
                .Property(e => e.product_index)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail_split>()
                .Property(e => e.wms_instore_detail_split_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail_split>()
                .Property(e => e.wms_instore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail_split>()
                .Property(e => e.product_number_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail_split>()
                .Property(e => e.storage_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail_split>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail_split>()
                .Property(e => e.wms_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_detail_split>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_status>()
                .Property(e => e.wms_instore_status_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_instore_status>()
                .Property(e => e.wms_instore_status_value)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.wms_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.wms_outstore_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.source_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.create_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.update_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.split_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.finish_user_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.wms_outstore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.wms_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.product_index)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.recipe_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.source_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.useable_batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_pick>()
                .Property(e => e.wms_outstore_detail_pick_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_pick>()
                .Property(e => e.wms_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_pick>()
                .Property(e => e.wms_outstore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_pick>()
                .Property(e => e.store_batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_pick>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_split>()
                .Property(e => e.wms_outstore_detail_split_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_split>()
                .Property(e => e.wms_outstore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_split>()
                .Property(e => e.wms_outstore_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_split>()
                .Property(e => e.wms_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_split>()
                .Property(e => e.remark)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_split>()
                .Property(e => e.product_index)
                .IsUnicode(false);

            modelBuilder.Entity<wms_outstore_detail_split>()
                .Property(e => e.car_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.wms_store_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.warehouse_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.storage_basket_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.wms_instore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.store_batch_code)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.trader_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.inspection_number)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.custom_number)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.lock_status)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.user_group_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.product_index)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.quality_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.currency_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.yy_product_id)
                .IsUnicode(false);

            modelBuilder.Entity<wms_store>()
                .Property(e => e.wms_outstore_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<yinshang>()
                .Property(e => e.sale_order_code)
                .IsUnicode(false);

            modelBuilder.Entity<yinshang>()
                .Property(e => e.id_card)
                .IsUnicode(false);

            modelBuilder.Entity<yinshang>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<yinshang>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<yinshang>()
                .Property(e => e.pay_number)
                .IsUnicode(false);

            modelBuilder.Entity<yinshang>()
                .Property(e => e.dealer_shop_id)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.yy_product_id)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.yy_product_code)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.yy_product_name)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.yy_product_zh_name)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.private_code)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.batch_number)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.product_id)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.product_level_id)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.quality_detail_id)
                .IsUnicode(false);

            modelBuilder.Entity<yy_product>()
                .Property(e => e.product_index)
                .IsUnicode(false);
        }
    }
}
