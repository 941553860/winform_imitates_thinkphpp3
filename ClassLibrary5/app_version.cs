namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.app_version")]
    public partial class app_version
    {
        [Key]
        [StringLength(36)]
        public string app_version_id { get; set; }

        [Column("app_version")]
        [StringLength(100)]
        public string app_version1 { get; set; }

        public DateTime? update_time { get; set; }
    }
}
