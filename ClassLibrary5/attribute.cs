namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.attribute")]
    public partial class attribute
    {
        [Key]
        [StringLength(36)]
        public string attribute_id { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        [StringLength(100)]
        public string attribute_key { get; set; }

        [StringLength(1000)]
        public string attribute_value { get; set; }
    }
}
