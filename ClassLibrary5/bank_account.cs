namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.bank_account")]
    public partial class bank_account
    {
        [Key]
        [StringLength(36)]
        public string bank_account_id { get; set; }

        [StringLength(200)]
        public string bank_account_name { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string bank_account_info { get; set; }

        [StringLength(36)]
        public string currency_id { get; set; }

        public decimal? account_money { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public int? del_flag { get; set; }
    }
}
