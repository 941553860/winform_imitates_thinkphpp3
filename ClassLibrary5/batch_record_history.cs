namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.batch_record_history")]
    public partial class batch_record_history
    {
        [Key]
        [StringLength(36)]
        public string batch_record_history_id { get; set; }

        [StringLength(36)]
        public string user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(200)]
        public string batch_code { get; set; }

        [StringLength(200)]
        public string source_product_name { get; set; }

        [StringLength(200)]
        public string source_product_code { get; set; }

        [StringLength(200)]
        public string source_product_zh_name { get; set; }

        [Column(TypeName = "date")]
        public DateTime? source_produce_date { get; set; }

        [StringLength(200)]
        public string target_product_name { get; set; }

        [StringLength(200)]
        public string target_product_zh_name { get; set; }

        [StringLength(200)]
        public string target_product_code { get; set; }

        [Column(TypeName = "date")]
        public DateTime? target_produce_date { get; set; }
    }
}
