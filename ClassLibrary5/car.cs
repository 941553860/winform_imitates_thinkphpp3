namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.car")]
    public partial class car
    {
        [Key]
        [StringLength(36)]
        public string car_id { get; set; }

        [StringLength(36)]
        public string wms_outstore_id { get; set; }

        public int? car_no { get; set; }

        public decimal? outstore_weight { get; set; }

        public decimal? weighting_heavy { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        public int? del_flag { get; set; }

        public int? send_flag { get; set; }
    }
}
