namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.car_detail")]
    public partial class car_detail
    {
        [Key]
        [StringLength(36)]
        public string car_detail_id { get; set; }

        [StringLength(36)]
        public string car_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        public decimal? outstore_weight { get; set; }

        public decimal? weighting_heavy { get; set; }

        public decimal? outstore_number { get; set; }

        [StringLength(50)]
        public string batch_code { get; set; }
    }
}
