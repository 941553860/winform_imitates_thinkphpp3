namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.company")]
    public partial class company
    {
        [Key]
        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(100)]
        public string company_name { get; set; }

        [Column(TypeName = "date")]
        public DateTime? expire_date { get; set; }

        public int? del_flag { get; set; }
    }
}
