namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.company_warehouse")]
    public partial class company_warehouse
    {
        [Key]
        [StringLength(36)]
        public string company_warehouse_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(100)]
        public string access_name { get; set; }

        [StringLength(100)]
        public string access_code { get; set; }

        [StringLength(100)]
        public string user_code { get; set; }

        [StringLength(100)]
        public string user_password { get; set; }

        [StringLength(100)]
        public string private_key { get; set; }

        [StringLength(100)]
        public string token { get; set; }

        [StringLength(500)]
        public string access_url { get; set; }
    }
}
