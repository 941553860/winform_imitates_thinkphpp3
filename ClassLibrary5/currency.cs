namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.currency")]
    public partial class currency
    {
        [Key]
        [StringLength(36)]
        public string currency_id { get; set; }

        public DateTime update_time { get; set; }

        [Required]
        [StringLength(50)]
        public string currency_name { get; set; }

        public decimal? exchange_ratio { get; set; }

        [Required]
        [StringLength(50)]
        public string currency_eng_name { get; set; }

        public decimal f_buy_price { get; set; }

        public decimal m_buy_price { get; set; }

        public decimal f_sell_price { get; set; }

        public decimal m_sell_price { get; set; }

        public decimal bank_conversion_price { get; set; }

        public int del_flag { get; set; }

        [StringLength(5)]
        public string sign { get; set; }
    }
}
