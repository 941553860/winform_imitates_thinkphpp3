namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.customs_store")]
    public partial class customs_store
    {
        [Key]
        public long customs_store_id { get; set; }

        public long product_number_id { get; set; }

        public long trader_id { get; set; }

        public long dealer_shop_id { get; set; }

        public decimal? store_number { get; set; }

        public decimal? lock_number { get; set; }

        public decimal? b2b_lock_number { get; set; }

        public decimal? conference_price { get; set; }

        public bool del_flag { get; set; }

        public long? purchase_id { get; set; }

        public long? purchase_detail_id { get; set; }

        public long warehouse_part_id { get; set; }
    }
}
