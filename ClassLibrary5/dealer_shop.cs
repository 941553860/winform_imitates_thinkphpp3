namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.dealer_shop")]
    public partial class dealer_shop
    {
        [Key]
        [StringLength(100)]
        public string dealer_shop_id { get; set; }

        [StringLength(100)]
        public string dealer_shop_code { get; set; }

        [StringLength(100)]
        public string dealer_shop_name { get; set; }

        [StringLength(36)]
        public string trader_id { get; set; }

        [StringLength(36)]
        public string platform_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public int? del_flag { get; set; }

        [StringLength(100)]
        public string app_id { get; set; }

        [StringLength(100)]
        public string secret_key { get; set; }

        [StringLength(100)]
        public string session_id { get; set; }

        [StringLength(45)]
        public string ydz_id { get; set; }

        [StringLength(200)]
        public string taobao_user_nick { get; set; }

        [StringLength(2000)]
        public string token { get; set; }

        public int? is_guanyi { get; set; }
    }
}
