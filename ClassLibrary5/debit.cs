namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.debit")]
    public partial class debit
    {
        [Key]
        [StringLength(36)]
        public string debit_id { get; set; }

        [StringLength(100)]
        public string debit_code { get; set; }

        [StringLength(36)]
        public string bank_account_id { get; set; }

        [StringLength(36)]
        public string debit_type { get; set; }

        public decimal? debit_money { get; set; }

        public decimal? debit_common_money { get; set; }

        [StringLength(36)]
        public string debit_status { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string confirm_user_id { get; set; }

        public DateTime? confirm_time { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }
    }
}
