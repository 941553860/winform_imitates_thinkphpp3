namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.debit_status")]
    public partial class debit_status
    {
        [Key]
        [StringLength(50)]
        public string debit_status_id { get; set; }

        [StringLength(100)]
        public string debit_status_value { get; set; }

        public int? sort_order { get; set; }
    }
}
