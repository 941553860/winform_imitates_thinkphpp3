namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.debit_type")]
    public partial class debit_type
    {
        [Key]
        [StringLength(50)]
        public string debit_type_id { get; set; }

        [StringLength(100)]
        public string debit_type_value { get; set; }
    }
}
