namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.employee")]
    public partial class employee
    {
        [Key]
        [StringLength(36)]
        public string employee_id { get; set; }

        [StringLength(36)]
        public string department_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(100)]
        public string employee_name { get; set; }

        [StringLength(100)]
        public string mobile_phone { get; set; }

        public int? del_flag { get; set; }
    }
}
