namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.file_info")]
    public partial class file_info
    {
        [Key]
        [StringLength(36)]
        public string file_info_id { get; set; }

        [StringLength(500)]
        public string file_name { get; set; }

        [StringLength(500)]
        public string thumb_file_path { get; set; }

        [StringLength(500)]
        public string full_file_path { get; set; }
    }
}
