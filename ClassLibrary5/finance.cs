namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.finance")]
    public partial class finance
    {
        [Key]
        [StringLength(36)]
        public string finance_id { get; set; }

        public int? finance_status { get; set; }

        public int? finance_type { get; set; }

        public decimal? finance_money { get; set; }

        public decimal? finance_real_money { get; set; }

        public int? payment_days { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        [StringLength(200)]
        public string source_code { get; set; }

        [StringLength(200)]
        public string source_detail_code { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public sbyte? finance_count_type { get; set; }

        [Column(TypeName = "date")]
        public DateTime? finance_count_date { get; set; }

        [StringLength(36)]
        public string currency_id { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        public DateTime? finish_time { get; set; }
    }
}
