namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.finance_detail")]
    public partial class finance_detail
    {
        [Key]
        [StringLength(36)]
        public string finance_detail_id { get; set; }

        [StringLength(36)]
        public string finance_id { get; set; }

        public decimal? finance_detail_money { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public sbyte? del_flag { get; set; }

        [StringLength(36)]
        public string currency_id { get; set; }

        public decimal? exchange_ratio { get; set; }
    }
}
