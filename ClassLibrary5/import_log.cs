namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.import_log")]
    public partial class import_log
    {
        [Key]
        [StringLength(36)]
        public string import_log_id { get; set; }

        [StringLength(200)]
        public string import_log_name { get; set; }

        [StringLength(32)]
        public string import_type { get; set; }

        [StringLength(500)]
        public string import_log_info { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(100)]
        public string file_path { get; set; }

        [StringLength(36)]
        public string import_user_id { get; set; }

        public DateTime? import_time { get; set; }
    }
}
