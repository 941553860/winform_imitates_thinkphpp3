namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.logistics")]
    public partial class logistics
    {
        [Key]
        [StringLength(36)]
        public string logistics_id { get; set; }

        [StringLength(100)]
        public string logistics_code { get; set; }

        [StringLength(100)]
        public string logistics_name { get; set; }

        public int? del_flag { get; set; }

        [StringLength(100)]
        public string logistics_beibei_code { get; set; }
    }
}
