namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.order_item")]
    public partial class order_item
    {
        [Key]
        [StringLength(36)]
        public string order_item_id { get; set; }

        [StringLength(100)]
        public string order_item_code { get; set; }

        [StringLength(100)]
        public string order_item_name { get; set; }

        public decimal? order_item_number { get; set; }

        public decimal? order_item_price { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public int? del_flag { get; set; }
    }
}
