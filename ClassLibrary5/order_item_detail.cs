namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.order_item_detail")]
    public partial class order_item_detail
    {
        [Key]
        [StringLength(36)]
        public string order_item_detail_id { get; set; }

        [StringLength(36)]
        public string order_item_id { get; set; }

        [StringLength(36)]
        public string product_number_id { get; set; }

        public decimal? item_number { get; set; }

        public decimal? item_price { get; set; }
    }
}
