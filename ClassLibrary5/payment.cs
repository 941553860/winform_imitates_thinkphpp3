namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.payment")]
    public partial class payment
    {
        [Key]
        [StringLength(36)]
        public string payment_id { get; set; }

        [StringLength(100)]
        public string payment_code { get; set; }

        [StringLength(36)]
        public string bank_account_id { get; set; }

        [StringLength(36)]
        public string payment_type { get; set; }

        public decimal? payment_money { get; set; }

        public decimal? payment_common_money { get; set; }

        [StringLength(36)]
        public string payment_status { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string confirm_user_id { get; set; }

        public DateTime? confirm_time { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string target_account_info { get; set; }

        [StringLength(36)]
        public string target_currency_id { get; set; }
    }
}
