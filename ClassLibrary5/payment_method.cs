namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.payment_method")]
    public partial class payment_method
    {
        [Key]
        [StringLength(36)]
        public string payment_method_id { get; set; }

        [StringLength(100)]
        public string payment_method_code { get; set; }

        [StringLength(100)]
        public string payment_method_name { get; set; }

        public int? del_flag { get; set; }

        [StringLength(100)]
        public string payment_method_pdd_code { get; set; }
    }
}
