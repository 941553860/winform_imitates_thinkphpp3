namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.platform")]
    public partial class platform
    {
        [Key]
        [StringLength(36)]
        public string platform_id { get; set; }

        [StringLength(100)]
        public string platform_code { get; set; }

        [StringLength(200)]
        public string platform_name { get; set; }

        public int? del_flag { get; set; }
    }
}
