namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.produce_plan")]
    public partial class produce_plan
    {
        [Key]
        [StringLength(36)]
        public string produce_plan_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [Required]
        [StringLength(100)]
        public string produce_plan_code { get; set; }

        [StringLength(36)]
        public string target_storage_basket_id { get; set; }

        public int? produce_plan_status { get; set; }

        [Column(TypeName = "date")]
        public DateTime? produce_plan_time { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? start_time { get; set; }

        [StringLength(36)]
        public string start_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? update_time { get; set; }

        public int? produce_meterial_status { get; set; }

        public int? produce_instore_status { get; set; }

        [StringLength(200)]
        public string remark { get; set; }

        public bool? del_flag { get; set; }
    }
}
