namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.produce_plan_detail")]
    public partial class produce_plan_detail
    {
        [Key]
        [StringLength(36)]
        public string produce_plan_detail_id { get; set; }

        [Required]
        [StringLength(36)]
        public string produce_plan_id { get; set; }

        [Required]
        [StringLength(36)]
        public string product_id { get; set; }

        public decimal? produce_plan_detail_weight { get; set; }

        public decimal? real_outstore_weight { get; set; }

        public decimal? real_instore_weight { get; set; }

        public decimal? real_money { get; set; }

        public decimal? loss_weight { get; set; }

        public decimal? loss_money { get; set; }

        [StringLength(200)]
        public string remark { get; set; }

        [StringLength(36)]
        public string target_storage_basket_id { get; set; }
    }
}
