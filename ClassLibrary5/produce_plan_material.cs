namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.produce_plan_material")]
    public partial class produce_plan_material
    {
        [Key]
        [StringLength(36)]
        public string produce_plan_material_id { get; set; }

        [Required]
        [StringLength(36)]
        public string produce_plan_id { get; set; }

        [StringLength(36)]
        public string wms_outstore_id { get; set; }

        [StringLength(200)]
        public string produce_plan_material_code { get; set; }

        public int? produce_plan_material_status { get; set; }

        public decimal? total_weight { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public int? del_flag { get; set; }

        [Column(TypeName = "date")]
        public DateTime? feed_time { get; set; }
    }
}
