namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.produce_plan_material_detail")]
    public partial class produce_plan_material_detail
    {
        [Key]
        [StringLength(36)]
        public string produce_plan_material_detail_id { get; set; }

        [Required]
        [StringLength(36)]
        public string produce_plan_material_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        public decimal? material_weight { get; set; }

        public int? del_flag { get; set; }

        public decimal? real_outstore_weight { get; set; }
    }
}
