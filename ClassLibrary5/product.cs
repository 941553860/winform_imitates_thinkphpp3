namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product")]
    public partial class product
    {
        [Key]
        [StringLength(36)]
        public string product_id { get; set; }

        [Required]
        [StringLength(100)]
        public string product_code { get; set; }

        [StringLength(100)]
        public string product_bar_code { get; set; }

        [Required]
        [StringLength(100)]
        public string product_name { get; set; }

        [StringLength(200)]
        public string product_zh_name { get; set; }

        [StringLength(500)]
        public string product_info { get; set; }

        [StringLength(1073741823)]
        public string product_remark { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public int? product_type { get; set; }

        public int? del_flag { get; set; }

        public decimal? product_weight { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(200)]
        public string private_code { get; set; }

        public int? product_quality_status { get; set; }

        public int? quality_level { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        [StringLength(200)]
        public string product_index { get; set; }

        [StringLength(200)]
        public string package_mode { get; set; }

        [StringLength(36)]
        public string unit_id { get; set; }

        public decimal? unit_conversion { get; set; }
    }
}
