namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product_bom")]
    public partial class product_bom
    {
        [Key]
        [StringLength(36)]
        public string product_bom_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string product_bom_detail_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }
    }
}
