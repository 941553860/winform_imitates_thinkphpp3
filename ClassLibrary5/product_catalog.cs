namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product_catalog")]
    public partial class product_catalog
    {
        [Key]
        [StringLength(36)]
        public string product_catalog_id { get; set; }

        [StringLength(100)]
        public string product_catalog_code { get; set; }

        [StringLength(100)]
        public string product_catalog_name { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public int? del_flag { get; set; }
    }
}
