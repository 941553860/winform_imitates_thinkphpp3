namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product_detail")]
    public partial class product_detail
    {
        [Key]
        [StringLength(36)]
        public string product_detail_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(20)]
        public string product_detail_code { get; set; }

        public decimal? product_detail_percentage { get; set; }

        [StringLength(100)]
        public string product_detail_name { get; set; }

        public decimal? product_detail_weight { get; set; }

        [StringLength(36)]
        public string meterial_id { get; set; }
    }
}
