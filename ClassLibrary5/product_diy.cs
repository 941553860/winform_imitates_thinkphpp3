namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product_diy")]
    public partial class product_diy
    {
        [Key]
        [StringLength(36)]
        public string product_diy_id { get; set; }

        [Required]
        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(100)]
        public string product_col_name { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public int? del_flag { get; set; }

        [StringLength(200)]
        public string defult_value { get; set; }
    }
}
