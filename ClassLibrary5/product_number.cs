namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product_number")]
    public partial class product_number
    {
        [Key]
        [StringLength(36)]
        public string product_number_id { get; set; }

        [StringLength(100)]
        public string product_number_code { get; set; }

        [StringLength(100)]
        public string product_number_name { get; set; }

        [StringLength(100)]
        public string product_number_eng_name { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public int? del_flag { get; set; }

        [StringLength(36)]
        public string product_number_status { get; set; }

        [StringLength(40)]
        public string hs_code { get; set; }

        public decimal? net_weight { get; set; }

        public decimal? gross_weight { get; set; }

        public decimal? length { get; set; }

        public decimal? width { get; set; }

        public decimal? height { get; set; }

        [StringLength(36)]
        public string product_main_picture { get; set; }

        [StringLength(36)]
        public string product_chn_mark_picture { get; set; }

        [StringLength(36)]
        public string product_attach1 { get; set; }

        [StringLength(36)]
        public string product_attach2 { get; set; }

        [StringLength(36)]
        public string product_attach3 { get; set; }

        [StringLength(36)]
        public string product_attach4 { get; set; }

        [StringLength(36)]
        public string product_attach5 { get; set; }

        [StringLength(100)]
        public string tariff_number { get; set; }

        [StringLength(36)]
        public string hg_status { get; set; }

        [StringLength(36)]
        public string gj_status { get; set; }

        public decimal? tariff_tax { get; set; }

        public decimal? added_value_tax { get; set; }

        public decimal? consumption_duty { get; set; }

        public decimal? total_tax_ratio { get; set; }

        [StringLength(100)]
        public string product_property { get; set; }

        [StringLength(100)]
        public string product_unit { get; set; }

        public decimal? product_tax { get; set; }

        [StringLength(36)]
        public string business_type { get; set; }

        [StringLength(200)]
        public string product_supplier { get; set; }

        [StringLength(36)]
        public string product_brand_id { get; set; }

        [StringLength(36)]
        public string product_origin_id { get; set; }

        [StringLength(200)]
        public string product_bar_code { get; set; }
    }
}
