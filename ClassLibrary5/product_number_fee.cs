namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product_number_fee")]
    public partial class product_number_fee
    {
        [Key]
        [StringLength(100)]
        public string product_number_fee_id { get; set; }

        [StringLength(100)]
        public string product_number_code { get; set; }

        [StringLength(100)]
        public string product_number_id { get; set; }

        [StringLength(100)]
        public string trader_id { get; set; }

        public decimal? post_fee { get; set; }

        public decimal? operation_fee { get; set; }

        public decimal? settlement_fee { get; set; }

        public decimal? weight { get; set; }

        [Column(TypeName = "date")]
        public DateTime? start_date { get; set; }

        public int? del_flag { get; set; }
    }
}
