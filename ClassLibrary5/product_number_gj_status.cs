namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.product_number_gj_status")]
    public partial class product_number_gj_status
    {
        [Key]
        [StringLength(36)]
        public string product_number_gj_status_id { get; set; }

        [StringLength(100)]
        public string product_number_gj_status_value { get; set; }

        public int? sort_order { get; set; }
    }
}
