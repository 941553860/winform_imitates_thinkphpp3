namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.province")]
    public partial class province
    {
        [Key]
        public long province_id { get; set; }

        [Required]
        [StringLength(100)]
        public string province_name { get; set; }

        public int del_flag { get; set; }
    }
}
