namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.purchase")]
    public partial class purchase
    {
        [Key]
        [StringLength(36)]
        public string purchase_id { get; set; }

        [StringLength(100)]
        public string purchase_code { get; set; }

        [StringLength(36)]
        public string supplier_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? purchase_time { get; set; }

        public decimal? bond_cash { get; set; }

        public decimal? payment_days { get; set; }

        public int? purchase_status { get; set; }

        public int? bond_cash_status { get; set; }

        public int? purchase_instore_status { get; set; }

        public decimal? contact_money { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string confirm_user_id { get; set; }

        public DateTime? confirm_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public int? del_flag { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(36)]
        public string currency_id { get; set; }

        public sbyte? purchase_finance_method { get; set; }

        public decimal? total_weight { get; set; }
    }
}
