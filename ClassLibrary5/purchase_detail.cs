namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.purchase_detail")]
    public partial class purchase_detail
    {
        [Key]
        [StringLength(36)]
        public string purchase_detail_id { get; set; }

        [StringLength(36)]
        public string purchase_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        public decimal? purchase_number { get; set; }

        public decimal? purchase_weight { get; set; }

        public decimal? purchase_price { get; set; }

        public decimal? instore_weight { get; set; }

        [StringLength(200)]
        public string product_index { get; set; }

        public int? del_flag { get; set; }

        [StringLength(200)]
        public string remark { get; set; }

        [StringLength(36)]
        public string currency_id { get; set; }
    }
}
