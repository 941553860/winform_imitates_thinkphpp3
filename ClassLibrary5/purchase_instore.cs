namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.purchase_instore")]
    public partial class purchase_instore
    {
        [Key]
        [StringLength(36)]
        public string purchase_instore_id { get; set; }

        [StringLength(36)]
        public string purchase_id { get; set; }

        [StringLength(100)]
        public string purchase_instore_code { get; set; }

        public int? purchase_instore_status { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(36)]
        public string wms_instore_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? purchase_arrive_time { get; set; }

        public decimal? instore_weight { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public int? del_flag { get; set; }

        public decimal? fee { get; set; }

        public sbyte? fee_status { get; set; }

        public decimal? deposit { get; set; }

        public decimal? advance { get; set; }

        public bool? advance_status { get; set; }
    }
}
