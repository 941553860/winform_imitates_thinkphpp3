namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.purchase_instore_detail")]
    public partial class purchase_instore_detail
    {
        [Key]
        [StringLength(36)]
        public string purchase_instore_detail_id { get; set; }

        [StringLength(36)]
        public string purchase_instore_id { get; set; }

        [StringLength(36)]
        public string purchase_detail_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string product_level_id { get; set; }

        [StringLength(200)]
        public string product_level_name { get; set; }

        public decimal? station_weight { get; set; }

        public decimal? instore_price { get; set; }

        public decimal? instore_number { get; set; }

        public decimal? instore_weight { get; set; }

        public decimal? loss_weight { get; set; }

        public int? del_flag { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }
    }
}
