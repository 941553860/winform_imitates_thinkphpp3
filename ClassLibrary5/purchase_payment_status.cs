namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.purchase_payment_status")]
    public partial class purchase_payment_status
    {
        [Key]
        [StringLength(50)]
        public string purchase_payment_status_id { get; set; }

        [StringLength(100)]
        public string purchase_payment_status_value { get; set; }

        public int? sort_order { get; set; }
    }
}
