namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.quality_detail_report_image")]
    public partial class quality_detail_report_image
    {
        [Key]
        [StringLength(36)]
        public string report_image_id { get; set; }

        [StringLength(36)]
        public string quality_detail_id { get; set; }

        [StringLength(500)]
        public string report_image_url { get; set; }

        public int? del_flag { get; set; }
    }
}
