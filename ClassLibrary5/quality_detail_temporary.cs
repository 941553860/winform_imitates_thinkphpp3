namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.quality_detail_temporary")]
    public partial class quality_detail_temporary
    {
        [Key]
        [StringLength(36)]
        public string quality_detail_id { get; set; }

        [StringLength(36)]
        public string quality_id { get; set; }

        [StringLength(100)]
        public string batch_code { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public int? del_flag { get; set; }

        [StringLength(36)]
        public string wms_store_id { get; set; }

        [StringLength(36)]
        public string product_level_id { get; set; }

        public int? quality_detail_status { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string quality_result_image_url { get; set; }
    }
}
