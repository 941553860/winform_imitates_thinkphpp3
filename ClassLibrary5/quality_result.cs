namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.quality_result")]
    public partial class quality_result
    {
        [Key]
        [StringLength(36)]
        public string quality_result_id { get; set; }

        [Required]
        [StringLength(36)]
        public string quality_detail_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string source_code { get; set; }

        [StringLength(36)]
        public string product_diy_id { get; set; }

        [Column("quality_result")]
        [StringLength(100)]
        public string quality_result1 { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }
    }
}
