namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.quality_temporary")]
    public partial class quality_temporary
    {
        [Key]
        [StringLength(36)]
        public string quality_id { get; set; }

        [Required]
        [StringLength(100)]
        public string quality_code { get; set; }

        public int? quality_status { get; set; }

        public int? quality_type { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        public int? del_flag { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }
    }
}
