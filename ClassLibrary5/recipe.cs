namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.recipe")]
    public partial class recipe
    {
        [Key]
        [StringLength(36)]
        public string recipe_id { get; set; }

        [StringLength(200)]
        public string recipe_code { get; set; }

        [StringLength(200)]
        public string recipe_name { get; set; }

        public long? create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        public int del_flag { get; set; }

        public int? quality_level { get; set; }
    }
}
