namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.recipe_detail")]
    public partial class recipe_detail
    {
        [Key]
        [StringLength(36)]
        public string recipe_detail_id { get; set; }

        [StringLength(36)]
        public string recipe_id { get; set; }

        [StringLength(200)]
        public string meterial_code { get; set; }

        [StringLength(200)]
        public string meterial_name { get; set; }

        public decimal? meterial_percentage { get; set; }
    }
}
