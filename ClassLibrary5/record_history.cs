namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.record_history")]
    public partial class record_history
    {
        [Key]
        [StringLength(36)]
        public string record_history_id { get; set; }

        [StringLength(36)]
        public string user_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }
    }
}
