namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.role")]
    public partial class role
    {
        [Key]
        [StringLength(36)]
        public string role_id { get; set; }

        [StringLength(100)]
        public string role_code { get; set; }

        [StringLength(100)]
        public string role_name { get; set; }

        public int? del_flag { get; set; }

        [StringLength(36)]
        public string role_group_id { get; set; }
    }
}
