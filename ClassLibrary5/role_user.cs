namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.role_user")]
    public partial class role_user
    {
        [Key]
        [StringLength(36)]
        public string role_user_id { get; set; }

        [StringLength(36)]
        public string role_id { get; set; }

        [StringLength(36)]
        public string user_id { get; set; }
    }
}
