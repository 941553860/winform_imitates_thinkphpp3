namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_customer")]
    public partial class sale_customer
    {
        [Key]
        [StringLength(36)]
        public string sale_customer_id { get; set; }

        [StringLength(200)]
        public string sale_customer_code { get; set; }

        [StringLength(200)]
        public string sale_customer_name { get; set; }

        [StringLength(200)]
        public string contact_name { get; set; }

        [StringLength(200)]
        public string contact_phone { get; set; }

        [StringLength(200)]
        public string mobile_phone { get; set; }

        [StringLength(500)]
        public string contact_address { get; set; }

        public decimal? credit_line { get; set; }

        public DateTime? create_time { get; set; }

        public int? del_flag { get; set; }
    }
}
