namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order")]
    public partial class sale_order
    {
        [Key]
        [StringLength(36)]
        public string sale_order_id { get; set; }

        [StringLength(36)]
        public string sale_customer_id { get; set; }

        [StringLength(100)]
        public string sale_order_code { get; set; }

        public int? sale_order_status { get; set; }

        public int? logistics_status { get; set; }

        public int? sale_pay_status { get; set; }

        public int? sale_order_outstore_status { get; set; }

        public decimal? total_money { get; set; }

        public decimal? total_weight { get; set; }

        [StringLength(45)]
        public string has_invoice { get; set; }

        [StringLength(500)]
        public string invoice_name { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? update_time { get; set; }

        public int? del_flag { get; set; }

        [StringLength(300)]
        public string remark { get; set; }

        public decimal? deposit { get; set; }

        public decimal? advance { get; set; }

        public int? sale_pay_type { get; set; }

        public decimal? bond_cash { get; set; }

        public int? bond_cash_status { get; set; }

        public decimal? payment_day { get; set; }
    }
}
