namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_detail")]
    public partial class sale_order_detail
    {
        [Key]
        [StringLength(36)]
        public string sale_order_detail_id { get; set; }

        [Required]
        [StringLength(36)]
        public string sale_order_id { get; set; }

        [Required]
        [StringLength(100)]
        public string product_id { get; set; }

        public decimal sale_price { get; set; }

        public decimal sale_weight { get; set; }

        public decimal? total_money { get; set; }

        [StringLength(200)]
        public string remark { get; set; }

        public int? del_flag { get; set; }

        [StringLength(200)]
        public string product_index { get; set; }
    }
}
