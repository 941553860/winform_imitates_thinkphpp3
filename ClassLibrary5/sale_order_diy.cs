namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_diy")]
    public partial class sale_order_diy
    {
        [Key]
        [StringLength(36)]
        public string sale_order_diy_id { get; set; }

        [StringLength(36)]
        public string sale_order_outstore_detail_id { get; set; }

        [StringLength(200)]
        public string diy_product_name { get; set; }

        [Column(TypeName = "date")]
        public DateTime? diy_produce_date { get; set; }
    }
}
