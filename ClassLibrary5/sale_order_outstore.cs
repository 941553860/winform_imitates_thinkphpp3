namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_outstore")]
    public partial class sale_order_outstore
    {
        [Key]
        [StringLength(36)]
        public string sale_order_outstore_id { get; set; }

        [Required]
        [StringLength(36)]
        public string sale_order_id { get; set; }

        [Required]
        [StringLength(100)]
        public string sale_order_outstore_code { get; set; }

        public int? sale_order_outstore_status { get; set; }

        [Column(TypeName = "date")]
        public DateTime? sale_order_outstore_send_time { get; set; }

        public decimal? total_outstore_weight { get; set; }

        public decimal? total_money { get; set; }

        [StringLength(300)]
        public string remark { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        [StringLength(36)]
        public string wms_outstore_id { get; set; }

        public int? del_flag { get; set; }

        public decimal? fee { get; set; }

        public decimal? advance { get; set; }

        public decimal? deposit { get; set; }
    }
}
