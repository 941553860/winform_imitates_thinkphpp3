namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_outstore_detail")]
    public partial class sale_order_outstore_detail
    {
        [Key]
        [StringLength(36)]
        public string sale_order_outstore_detail_id { get; set; }

        [Required]
        [StringLength(36)]
        public string sale_order_outstore_id { get; set; }

        [Required]
        [StringLength(36)]
        public string product_id { get; set; }

        [Required]
        [StringLength(36)]
        public string sale_order_detail_id { get; set; }

        public decimal? outstore_weight { get; set; }

        public decimal? outstore_price { get; set; }

        [StringLength(100)]
        public string remark { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public int? del_flag { get; set; }

        public decimal? station_weight { get; set; }

        public decimal? real_outstore_weight { get; set; }
    }
}
