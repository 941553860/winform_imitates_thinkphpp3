namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_promotion")]
    public partial class sale_order_promotion
    {
        [Key]
        public long sale_order_promotion_id { get; set; }

        [StringLength(50)]
        public string sale_order_id { get; set; }

        public decimal? promotion_amount { get; set; }

        [StringLength(1000)]
        public string promotion_remark { get; set; }
    }
}
