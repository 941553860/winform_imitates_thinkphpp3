namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_status")]
    public partial class sale_order_status
    {
        [Key]
        [StringLength(36)]
        public string sale_order_status_id { get; set; }

        [StringLength(100)]
        public string sale_order_status_value { get; set; }

        public int? sort_order { get; set; }
    }
}
