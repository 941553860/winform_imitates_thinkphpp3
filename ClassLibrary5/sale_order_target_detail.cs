namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_target_detail")]
    public partial class sale_order_target_detail
    {
        [Key]
        [StringLength(36)]
        public string sale_order_target_detail_id { get; set; }

        [Required]
        [StringLength(36)]
        public string sale_order_id { get; set; }

        [Required]
        [StringLength(36)]
        public string product_number_id { get; set; }

        public decimal sale_price { get; set; }

        public decimal sale_number { get; set; }

        public decimal? sale_sub_total { get; set; }

        public decimal? tariff_amount { get; set; }

        public decimal? added_value_amount { get; set; }

        public decimal? consumption_duty_amount { get; set; }

        public decimal sale_total { get; set; }
    }
}
