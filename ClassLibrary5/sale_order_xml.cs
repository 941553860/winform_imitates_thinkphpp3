namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_order_xml")]
    public partial class sale_order_xml
    {
        [Key]
        public long sale_order_xml_id { get; set; }

        [Column("sale_order_xml")]
        [StringLength(1073741823)]
        public string sale_order_xml1 { get; set; }

        public DateTime? create_time { get; set; }
    }
}
