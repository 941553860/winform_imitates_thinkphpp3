namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_refund")]
    public partial class sale_refund
    {
        [Key]
        [StringLength(100)]
        public string sale_refund_id { get; set; }

        [StringLength(100)]
        public string sale_order_id { get; set; }

        [StringLength(100)]
        public string sale_order_code { get; set; }

        [StringLength(100)]
        public string sale_refund_code { get; set; }

        [StringLength(100)]
        public string platform_id { get; set; }

        [StringLength(100)]
        public string platform_name { get; set; }

        [StringLength(100)]
        public string dealer_shop_id { get; set; }

        [StringLength(200)]
        public string dealer_shop_name { get; set; }

        [StringLength(100)]
        public string sale_refund_status { get; set; }

        [StringLength(500)]
        public string refund_reason { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string refund_remark { get; set; }

        public decimal? refund_money { get; set; }

        public decimal? refund_total_money { get; set; }

        public decimal? refund_dis_money { get; set; }

        public decimal? refund_number { get; set; }

        [StringLength(100)]
        public string logistics_id { get; set; }

        [StringLength(50)]
        public string logistics_number { get; set; }

        public DateTime? create_time { get; set; }

        public DateTime? sync_time { get; set; }
    }
}
