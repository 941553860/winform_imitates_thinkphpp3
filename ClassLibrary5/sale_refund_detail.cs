namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sale_refund_detail")]
    public partial class sale_refund_detail
    {
        [Key]
        [StringLength(100)]
        public string sale_refund_detail_id { get; set; }

        [StringLength(100)]
        public string sale_refund_id { get; set; }

        [StringLength(100)]
        public string sale_product_id { get; set; }

        [StringLength(100)]
        public string sale_goods_name { get; set; }

        public decimal? sale_goods_price { get; set; }

        public decimal? refund_product_number { get; set; }

        public decimal? refund_fee { get; set; }

        public decimal? refund_total_fee { get; set; }

        public decimal? refund_dis_fee { get; set; }
    }
}
