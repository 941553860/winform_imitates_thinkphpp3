namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.storage_basket")]
    public partial class storage_basket
    {
        [Key]
        [StringLength(36)]
        public string storage_basket_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(36)]
        public string storage_rack_level_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(100)]
        public string storage_basket_code { get; set; }

        [StringLength(100)]
        public string storage_basket_name { get; set; }

        [StringLength(36)]
        public string storage_basket_type { get; set; }

        public int? del_flag { get; set; }

        [StringLength(200)]
        public string position_code { get; set; }
    }
}
