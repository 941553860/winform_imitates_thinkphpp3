namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.supplier")]
    public partial class supplier
    {
        [Key]
        [StringLength(36)]
        public string supplier_id { get; set; }

        [StringLength(100)]
        public string supplier_code { get; set; }

        [StringLength(300)]
        public string supplier_name { get; set; }

        [StringLength(100)]
        public string contact_name { get; set; }

        [StringLength(200)]
        public string contact_phone { get; set; }

        [StringLength(300)]
        public string contact_address { get; set; }

        public int? del_flag { get; set; }
    }
}
