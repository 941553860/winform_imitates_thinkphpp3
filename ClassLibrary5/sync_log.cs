namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sync_log")]
    public partial class sync_log
    {
        [Key]
        public long sync_log_id { get; set; }

        public long sync_type_id { get; set; }

        public DateTime sync_begin_time { get; set; }

        public DateTime sync_end_time { get; set; }

        [Column(TypeName = "text")]
        [Required]
        [StringLength(65535)]
        public string sync_content { get; set; }
    }
}
