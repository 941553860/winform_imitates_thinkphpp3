namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.sync_type")]
    public partial class sync_type
    {
        [Key]
        public long sync_type_id { get; set; }

        [StringLength(50)]
        public string sync_type_name { get; set; }

        [StringLength(100)]
        public string sync_frequence { get; set; }

        public int? sync_status { get; set; }

        public DateTime? last_sync_time { get; set; }

        public DateTime? check_time1 { get; set; }

        public DateTime? check_time2 { get; set; }

        public int? del_flag { get; set; }
    }
}
