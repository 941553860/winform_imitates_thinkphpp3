namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.trader")]
    public partial class trader
    {
        [Key]
        [StringLength(36)]
        public string trader_id { get; set; }

        [StringLength(100)]
        public string trader_code { get; set; }

        [StringLength(100)]
        public string trader_name { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(36)]
        public string trader_type_id { get; set; }

        public int? del_flag { get; set; }
    }
}
