namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.transform_item")]
    public partial class transform_item
    {
        [Key]
        [StringLength(50)]
        public string transform_item_id { get; set; }

        [Required]
        [StringLength(100)]
        public string transform_item_code { get; set; }

        [Required]
        [StringLength(200)]
        public string transform_item_name { get; set; }

        [StringLength(50)]
        public string trader_id { get; set; }

        [Required]
        [StringLength(50)]
        public string dealer_shop_id { get; set; }

        public decimal? transform_item_price { get; set; }

        public decimal? transform_item_number { get; set; }

        public decimal? transform_item_type_number { get; set; }

        public DateTime? create_time { get; set; }

        public int? del_flag { get; set; }

        [StringLength(50)]
        public string company_id { get; set; }
    }
}
