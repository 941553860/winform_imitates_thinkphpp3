namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.transform_item_detail")]
    public partial class transform_item_detail
    {
        [Key]
        [StringLength(50)]
        public string transform_item_detail_id { get; set; }

        [Required]
        [StringLength(50)]
        public string transform_item_id { get; set; }

        [Required]
        [StringLength(50)]
        public string product_number_id { get; set; }

        [Required]
        [StringLength(100)]
        public string product_number_code { get; set; }

        [StringLength(100)]
        public string item_name { get; set; }

        public decimal item_number { get; set; }

        public decimal item_price { get; set; }

        [StringLength(50)]
        public string company_id { get; set; }
    }
}
