namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.transform_order_close")]
    public partial class transform_order_close
    {
        [Key]
        public long transform_order_id { get; set; }

        [StringLength(100)]
        public string transform_order_code { get; set; }

        public int? transform_order_type { get; set; }

        [StringLength(100)]
        public string sale_order_code { get; set; }

        [StringLength(100)]
        public string mft_number { get; set; }

        public long? platform_id { get; set; }

        [StringLength(100)]
        public string platform_name { get; set; }

        public long? dealer_shop_id { get; set; }

        [StringLength(100)]
        public string dealer_shop_name { get; set; }

        [StringLength(100)]
        public string oto_code { get; set; }

        public int? package_flag { get; set; }

        public decimal? post_fee { get; set; }

        public decimal? insurance_fee { get; set; }

        public decimal? amount { get; set; }

        [StringLength(100)]
        public string buyer_account { get; set; }

        [StringLength(100)]
        public string phone { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        public decimal? tax_amount { get; set; }

        public decimal? tariff_amount { get; set; }

        public decimal? added_value_tax_amount { get; set; }

        public decimal? consumption_duty_amount { get; set; }

        public decimal? gross_weight { get; set; }

        public decimal? dis_amount { get; set; }

        public DateTime? order_time { get; set; }

        public DateTime? pay_time { get; set; }

        [StringLength(100)]
        public string payment_code { get; set; }

        [StringLength(100)]
        public string payment_order_seq { get; set; }

        public long? payment_method_id { get; set; }

        [StringLength(100)]
        public string payment_method_name { get; set; }

        [StringLength(20)]
        public string id_card { get; set; }

        [StringLength(50)]
        public string buyer_name { get; set; }

        public long? logistics_id { get; set; }

        [StringLength(100)]
        public string logistics_name { get; set; }

        [StringLength(50)]
        public string logistics_number { get; set; }

        [StringLength(50)]
        public string consignee { get; set; }

        public long? province_id { get; set; }

        [StringLength(50)]
        public string province_name { get; set; }

        [StringLength(50)]
        public string city { get; set; }

        [StringLength(100)]
        public string district { get; set; }

        [StringLength(250)]
        public string consignee_addr { get; set; }

        [StringLength(50)]
        public string consignee_tel { get; set; }

        [StringLength(10)]
        public string zip_code { get; set; }

        [StringLength(1000)]
        public string goods_name { get; set; }

        public DateTime? transform_time { get; set; }

        public DateTime? create_time { get; set; }

        public int? deliver_status { get; set; }

        [StringLength(32)]
        public string mft_status { get; set; }

        [StringLength(1000)]
        public string mft_result { get; set; }

        [StringLength(2)]
        public string check_flag { get; set; }

        [StringLength(1000)]
        public string check_msg { get; set; }

        public int? transform_status { get; set; }

        [StringLength(500)]
        public string transform_status_msg { get; set; }

        [StringLength(200)]
        public string distribution_code { get; set; }

        public long? source_platform_id { get; set; }

        [StringLength(200)]
        public string source_platform_name { get; set; }

        public long? source_dealer_shop_id { get; set; }

        [StringLength(200)]
        public string source_dealer_shop_name { get; set; }

        public long? trader_id { get; set; }

        [StringLength(500)]
        public string detail_info { get; set; }

        public decimal? trader_package_weight { get; set; }

        public decimal? trader_total_weight { get; set; }

        public decimal? trader_deliver_fee { get; set; }

        public decimal? trader_product_fee { get; set; }

        public int? del_flag { get; set; }
    }
}
