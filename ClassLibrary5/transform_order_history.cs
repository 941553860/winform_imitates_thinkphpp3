namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.transform_order_history")]
    public partial class transform_order_history
    {
        [Key]
        public long transform_order_history_id { get; set; }

        public DateTime? time { get; set; }

        [StringLength(30)]
        public string user_code { get; set; }

        [StringLength(100)]
        public string transform_order_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        [StringLength(100)]
        public string sale_order_code { get; set; }

        public int? del_flag { get; set; }

        public int? type { get; set; }
    }
}
