namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.transform_order_info")]
    public partial class transform_order_info
    {
        [Key]
        public long transform_order_info_id { get; set; }

        [StringLength(100)]
        public string transform_order_id { get; set; }

        public DateTime? transform_order_time { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string transform_order_info_detail { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string transform_order_info_result { get; set; }
    }
}
