namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.transform_order_promotion")]
    public partial class transform_order_promotion
    {
        [Key]
        public long transform_order_promotion_id { get; set; }

        public long transform_order_id { get; set; }

        public decimal promotion_amount { get; set; }

        [Required]
        [StringLength(1000)]
        public string promotion_remark { get; set; }
    }
}
