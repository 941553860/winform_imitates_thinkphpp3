namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.transform_order_target_detail")]
    public partial class transform_order_target_detail
    {
        [Key]
        [StringLength(100)]
        public string transform_order_detail_id { get; set; }

        [Required]
        [StringLength(100)]
        public string transform_order_id { get; set; }

        [StringLength(100)]
        public string product_id { get; set; }

        [StringLength(100)]
        public string product_number_id { get; set; }

        [StringLength(100)]
        public string product_number_code { get; set; }

        [StringLength(100)]
        public string target_product_number_id { get; set; }

        [StringLength(100)]
        public string target_product_number_code { get; set; }

        [StringLength(200)]
        public string goods_name { get; set; }

        public decimal quality { get; set; }

        [StringLength(50)]
        public string unit { get; set; }

        public decimal decl_price { get; set; }

        public decimal decl_total { get; set; }

        public decimal? tax_amount { get; set; }

        public decimal? tariff { get; set; }

        public decimal? added_value_tax { get; set; }

        public decimal? consumption_duty { get; set; }
    }
}
