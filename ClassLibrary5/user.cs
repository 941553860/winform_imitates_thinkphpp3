namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.user")]
    public partial class user
    {
        [Key]
        [StringLength(36)]
        public string user_id { get; set; }

        [StringLength(36)]
        public string user_type_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(36)]
        public string trader_id { get; set; }

        [StringLength(36)]
        public string employee_id { get; set; }

        [StringLength(100)]
        public string user_code { get; set; }

        [StringLength(100)]
        public string user_name { get; set; }

        [StringLength(100)]
        public string user_password { get; set; }

        public int? del_flag { get; set; }

        [StringLength(10)]
        public string private_key { get; set; }

        [StringLength(36)]
        public string dealer_shop_id { get; set; }

        [StringLength(36)]
        public string department_id { get; set; }

        [StringLength(20)]
        public string user_phone { get; set; }
    }
}
