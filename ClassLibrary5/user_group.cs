namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.user_group")]
    public partial class user_group
    {
        [Key]
        [StringLength(36)]
        public string user_group_id { get; set; }

        [StringLength(100)]
        public string user_group_code { get; set; }

        [StringLength(100)]
        public string user_group_name { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }
    }
}
