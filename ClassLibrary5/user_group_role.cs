namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.user_group_role")]
    public partial class user_group_role
    {
        [Key]
        [StringLength(36)]
        public string user_group_role_id { get; set; }

        [StringLength(36)]
        public string user_group_id { get; set; }

        [StringLength(36)]
        public string role_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }
    }
}
