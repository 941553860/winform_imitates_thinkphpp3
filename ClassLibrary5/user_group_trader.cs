namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.user_group_trader")]
    public partial class user_group_trader
    {
        [Key]
        [StringLength(36)]
        public string user_group_trader_id { get; set; }

        [StringLength(36)]
        public string user_group_id { get; set; }

        [StringLength(36)]
        public string trader_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }
    }
}
