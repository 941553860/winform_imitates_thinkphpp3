namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.user_group_warehouse")]
    public partial class user_group_warehouse
    {
        [Key]
        [StringLength(36)]
        public string user_group_warehouse_id { get; set; }

        [StringLength(36)]
        public string user_group_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }
    }
}
