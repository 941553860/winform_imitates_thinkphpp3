namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.user_type")]
    public partial class user_type
    {
        [Key]
        [StringLength(36)]
        public string user_type_id { get; set; }

        [StringLength(100)]
        public string user_type_code { get; set; }

        [StringLength(100)]
        public string user_type_name { get; set; }

        public int? del_flag { get; set; }
    }
}
