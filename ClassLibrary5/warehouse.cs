namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.warehouse")]
    public partial class warehouse
    {
        [Key]
        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(100)]
        public string warehouse_code { get; set; }

        [StringLength(100)]
        public string warehouse_name { get; set; }

        [StringLength(36)]
        public string warehouse_type_id { get; set; }

        public int? del_flag { get; set; }
    }
}
