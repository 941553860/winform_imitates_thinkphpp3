namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.warehouse_change")]
    public partial class warehouse_change
    {
        [Key]
        [StringLength(36)]
        public string warehouse_change_id { get; set; }

        [StringLength(50)]
        public string warehouse_change_code { get; set; }

        public long? warehouse_change_status { get; set; }

        public decimal? warehouse_change_weight { get; set; }

        public DateTime? create_time { get; set; }

        public long? create_user_id { get; set; }

        public DateTime? confirm_time { get; set; }

        public long? confirm_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        public long? finish_user_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public long? del_flag { get; set; }
    }
}
