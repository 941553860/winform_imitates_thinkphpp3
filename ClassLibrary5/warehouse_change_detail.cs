namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.warehouse_change_detail")]
    public partial class warehouse_change_detail
    {
        [Key]
        [StringLength(36)]
        public string warehouse_change_detail_id { get; set; }

        [StringLength(36)]
        public string warehouse_change_id { get; set; }

        [StringLength(36)]
        public string wms_store_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(36)]
        public string storage_basket_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(200)]
        public string store_batch_code { get; set; }

        public decimal? product_price { get; set; }

        public decimal? store_weight { get; set; }

        public decimal? transform_weight { get; set; }

        [StringLength(36)]
        public string target_basket_id { get; set; }
    }
}
