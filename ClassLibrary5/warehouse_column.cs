namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.warehouse_column")]
    public partial class warehouse_column
    {
        [Key]
        [StringLength(36)]
        public string warehouse_column_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(36)]
        public string warehouse_area_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [StringLength(100)]
        public string warehouse_column_code { get; set; }

        [StringLength(100)]
        public string warehouse_column_name { get; set; }

        public int? del_flag { get; set; }
    }
}
