namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.warehouse_type")]
    public partial class warehouse_type
    {
        [Key]
        [StringLength(36)]
        public string warehouse_type_id { get; set; }

        [StringLength(100)]
        public string warehouse_type_code { get; set; }

        [StringLength(100)]
        public string warehouse_type_name { get; set; }

        public int? del_flag { get; set; }
    }
}
