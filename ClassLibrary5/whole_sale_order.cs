namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.whole_sale_order")]
    public partial class whole_sale_order
    {
        [Key]
        [StringLength(36)]
        public string whole_sale_order_id { get; set; }

        [StringLength(100)]
        public string whole_sale_order_code { get; set; }

        [StringLength(100)]
        public string receiver_name { get; set; }

        [StringLength(100)]
        public string receiver_phone { get; set; }

        [StringLength(500)]
        public string receiver_address { get; set; }

        [StringLength(100)]
        public string deliver_name { get; set; }

        [StringLength(100)]
        public string deliver_phone { get; set; }

        [StringLength(500)]
        public string deliver_address { get; set; }

        [StringLength(100)]
        public string contact_number { get; set; }

        [Column(TypeName = "date")]
        public DateTime? contact_sign_date { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        [StringLength(36)]
        public string trader_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public decimal? post_fee { get; set; }

        public decimal? insurance_fee { get; set; }

        public decimal? tax_amount { get; set; }

        public decimal? dis_amount { get; set; }

        [StringLength(36)]
        public string currency_id { get; set; }

        public decimal? exchange_ratio { get; set; }

        [StringLength(200)]
        public string invoice_name { get; set; }

        public decimal? item_number { get; set; }

        public decimal? buyer_payment { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string confirm_user_id { get; set; }

        public DateTime? confirm_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [Column(TypeName = "date")]
        public DateTime? require_deliver_date { get; set; }

        [StringLength(36)]
        public string whole_sale_order_status { get; set; }

        public decimal? debit_money { get; set; }

        public decimal? unconfirmed_money { get; set; }

        public decimal? undebit_money { get; set; }

        [StringLength(50)]
        public string whole_sale_order_debit_status { get; set; }
    }
}
