namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.whole_sale_order_detail")]
    public partial class whole_sale_order_detail
    {
        [Key]
        [StringLength(36)]
        public string whole_sale_order_detail_id { get; set; }

        [StringLength(36)]
        public string whole_sale_order_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(200)]
        public string contact_product_name { get; set; }

        public decimal? contact_number { get; set; }

        public decimal? contact_price { get; set; }

        public decimal? contact_base_price { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public decimal? finish_number { get; set; }

        public decimal? wait_outstore_number { get; set; }
    }
}
