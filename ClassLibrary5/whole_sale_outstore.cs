namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.whole_sale_outstore")]
    public partial class whole_sale_outstore
    {
        [Key]
        [StringLength(36)]
        public string whole_sale_outstore_id { get; set; }

        [StringLength(36)]
        public string whole_sale_order_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(100)]
        public string whole_sale_outstore_code { get; set; }

        [StringLength(36)]
        public string whole_sale_outstore_status { get; set; }

        public decimal? outstore_number { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string logistics_id { get; set; }

        [StringLength(100)]
        public string logistics_number { get; set; }

        [StringLength(100)]
        public string wms_outstore_code { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }
    }
}
