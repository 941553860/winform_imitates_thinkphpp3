namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.whole_sale_outstore_detail")]
    public partial class whole_sale_outstore_detail
    {
        [Key]
        [StringLength(36)]
        public string whole_sale_outstore_detail_id { get; set; }

        [StringLength(36)]
        public string whole_sale_outstore_id { get; set; }

        [StringLength(36)]
        public string whole_sale_order_detail_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        public decimal? outstore_number { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }
    }
}
