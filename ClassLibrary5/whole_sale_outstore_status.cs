namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.whole_sale_outstore_status")]
    public partial class whole_sale_outstore_status
    {
        [Key]
        [StringLength(36)]
        public string whole_sale_outstore_status_id { get; set; }

        [StringLength(100)]
        public string whole_sale_outstore_status_value { get; set; }

        public int? sort_order { get; set; }
    }
}
