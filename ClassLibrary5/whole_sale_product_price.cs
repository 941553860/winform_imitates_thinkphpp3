namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.whole_sale_product_price")]
    public partial class whole_sale_product_price
    {
        [Key]
        [StringLength(36)]
        public string whole_sale_product_price_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        public decimal? price { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? start_date { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? update_time { get; set; }
    }
}
