namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_check_store")]
    public partial class wms_check_store
    {
        [Key]
        [StringLength(36)]
        public string check_store_id { get; set; }

        [StringLength(50)]
        public string check_store_code { get; set; }

        [Column(TypeName = "date")]
        public DateTime? check_date { get; set; }

        [StringLength(36)]
        public string check_user_id { get; set; }

        public int? check_store_status { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string confirm_user_id { get; set; }

        public DateTime? confirm_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        public int? check_store_type { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public long? del_flag { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        public bool? check_type { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? update_time { get; set; }
    }
}
