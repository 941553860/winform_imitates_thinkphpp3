namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_check_store_detail")]
    public partial class wms_check_store_detail
    {
        [Key]
        [StringLength(36)]
        public string check_store_detail_id { get; set; }

        [StringLength(36)]
        public string check_store_id { get; set; }

        [StringLength(36)]
        public string wms_store_id { get; set; }

        public decimal? source_store_weight { get; set; }

        public decimal? target_store_weight { get; set; }

        [StringLength(200)]
        public string store_batch_code { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        public decimal? product_price { get; set; }

        public long? del_flag { get; set; }

        [StringLength(50)]
        public string position_code { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }
    }
}
