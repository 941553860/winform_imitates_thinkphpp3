namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_instore")]
    public partial class wms_instore
    {
        [Key]
        [StringLength(36)]
        public string wms_instore_id { get; set; }

        [StringLength(100)]
        public string wms_instore_code { get; set; }

        [StringLength(100)]
        public string source_instore_code { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        public int? wms_instore_status { get; set; }

        [Column(TypeName = "date")]
        public DateTime? wms_arrive_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? create_time { get; set; }

        [Required]
        [StringLength(36)]
        public string confirm_user_id { get; set; }

        public DateTime? confirm_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [StringLength(50)]
        public string source_code { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public int? instore_type { get; set; }

        public int? instore_inspect_status { get; set; }

        public decimal? instore_weight { get; set; }

        [StringLength(36)]
        public string trade_id { get; set; }

        public decimal? instore_number { get; set; }
    }
}
