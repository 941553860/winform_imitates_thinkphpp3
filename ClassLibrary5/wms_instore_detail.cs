namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_instore_detail")]
    public partial class wms_instore_detail
    {
        [Key]
        [StringLength(36)]
        public string wms_instore_detail_id { get; set; }

        [StringLength(36)]
        public string wms_instore_id { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string remark { get; set; }

        public decimal? instore_weight { get; set; }

        public decimal? real_instore_weight { get; set; }

        public int? finish_split { get; set; }

        [StringLength(36)]
        public string product_level_id { get; set; }

        [StringLength(36)]
        public string target_storage_basket_id { get; set; }

        [StringLength(200)]
        public string product_index { get; set; }

        public decimal? instore_number { get; set; }

        public decimal? real_instore_number { get; set; }

        public decimal? instore_price { get; set; }
    }
}
