namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_instore_detail_split")]
    public partial class wms_instore_detail_split
    {
        [Key]
        [StringLength(36)]
        public string wms_instore_detail_split_id { get; set; }

        [StringLength(36)]
        public string wms_instore_detail_id { get; set; }

        [StringLength(36)]
        public string product_number_id { get; set; }

        [StringLength(36)]
        public string storage_basket_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? expire_date { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public decimal? instore_split_weight { get; set; }

        public decimal? instore_split_number { get; set; }

        [StringLength(36)]
        public string wms_store_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }
    }
}
