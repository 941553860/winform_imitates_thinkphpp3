namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_instore_status")]
    public partial class wms_instore_status
    {
        [Key]
        [StringLength(100)]
        public string wms_instore_status_id { get; set; }

        [StringLength(100)]
        public string wms_instore_status_value { get; set; }

        public int? sort_order { get; set; }
    }
}
