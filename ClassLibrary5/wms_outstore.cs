namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_outstore")]
    public partial class wms_outstore
    {
        [Key]
        [StringLength(36)]
        public string wms_outstore_id { get; set; }

        [StringLength(36)]
        public string warehouse_id { get; set; }

        [StringLength(100)]
        public string wms_outstore_code { get; set; }

        public int? wms_outstore_status { get; set; }

        public decimal? outstore_weight { get; set; }

        public int? outstore_type { get; set; }

        [StringLength(50)]
        public string source_code { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        public DateTime? create_time { get; set; }

        [StringLength(36)]
        public string create_user_id { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(36)]
        public string update_user_id { get; set; }

        public DateTime? split_time { get; set; }

        [StringLength(36)]
        public string split_user_id { get; set; }

        public DateTime? finish_time { get; set; }

        [StringLength(36)]
        public string finish_user_id { get; set; }

        [StringLength(255)]
        public string remark { get; set; }

        public int? del_flag { get; set; }
    }
}
