namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_outstore_detail")]
    public partial class wms_outstore_detail
    {
        [Key]
        [StringLength(36)]
        public string wms_outstore_detail_id { get; set; }

        [Required]
        [StringLength(36)]
        public string wms_outstore_id { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string product_index { get; set; }

        [StringLength(36)]
        public string recipe_id { get; set; }

        public decimal? oustore_number { get; set; }

        public decimal? real_outstore_numebr { get; set; }

        public decimal? outstore_weight { get; set; }

        public decimal? real_outstore_weight { get; set; }

        [StringLength(36)]
        public string source_id { get; set; }

        [StringLength(200)]
        public string useable_batch_code { get; set; }

        [StringLength(200)]
        public string remark { get; set; }

        public int? del_flag { get; set; }

        public int? finish_split { get; set; }
    }
}
