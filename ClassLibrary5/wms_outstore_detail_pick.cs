namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_outstore_detail_pick")]
    public partial class wms_outstore_detail_pick
    {
        [Key]
        [StringLength(36)]
        public string wms_outstore_detail_pick_id { get; set; }

        [Required]
        [StringLength(36)]
        public string wms_outstore_id { get; set; }

        [Required]
        [StringLength(36)]
        public string wms_outstore_detail_id { get; set; }

        [StringLength(200)]
        public string store_batch_code { get; set; }

        [StringLength(200)]
        public string remark { get; set; }

        public DateTime? update_time { get; set; }
    }
}
