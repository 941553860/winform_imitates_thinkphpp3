namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_outstore_detail_split")]
    public partial class wms_outstore_detail_split
    {
        [Key]
        [StringLength(36)]
        public string wms_outstore_detail_split_id { get; set; }

        [Required]
        [StringLength(36)]
        public string wms_outstore_detail_id { get; set; }

        [StringLength(36)]
        public string wms_outstore_id { get; set; }

        [StringLength(36)]
        public string wms_store_id { get; set; }

        public decimal? outstore_weight { get; set; }

        public int? outstore_number { get; set; }

        [StringLength(200)]
        public string remark { get; set; }

        public DateTime? update_time { get; set; }

        [StringLength(200)]
        public string product_index { get; set; }

        [StringLength(36)]
        public string car_detail_id { get; set; }
    }
}
