namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.wms_store")]
    public partial class wms_store
    {
        [Key]
        [StringLength(36)]
        public string wms_store_id { get; set; }

        [Required]
        [StringLength(36)]
        public string warehouse_id { get; set; }

        [Required]
        [StringLength(36)]
        public string storage_basket_id { get; set; }

        [StringLength(36)]
        public string wms_instore_detail_id { get; set; }

        [Required]
        [StringLength(200)]
        public string store_batch_code { get; set; }

        [Column(TypeName = "date")]
        public DateTime instore_date { get; set; }

        [StringLength(36)]
        public string trader_id { get; set; }

        public decimal? outstore_lock_weight { get; set; }

        public int? del_flag { get; set; }

        [StringLength(100)]
        public string inspection_number { get; set; }

        [StringLength(100)]
        public string custom_number { get; set; }

        [StringLength(36)]
        public string lock_status { get; set; }

        [StringLength(36)]
        public string user_group_id { get; set; }

        [StringLength(36)]
        public string company_id { get; set; }

        public int? quality_status { get; set; }

        public decimal? store_weight { get; set; }

        public decimal? store_number { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string product_index { get; set; }

        public decimal? product_price { get; set; }

        [StringLength(36)]
        public string quality_detail_id { get; set; }

        public int? quality_detail_status { get; set; }

        [StringLength(36)]
        public string currency_id { get; set; }

        public decimal? exchange_ratio { get; set; }

        public decimal? instore_price { get; set; }

        [StringLength(36)]
        public string yy_product_id { get; set; }

        [StringLength(36)]
        public string wms_outstore_detail_id { get; set; }
    }
}
