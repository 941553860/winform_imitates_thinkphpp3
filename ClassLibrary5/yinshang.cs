namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.yinshang")]
    public partial class yinshang
    {
        [Key]
        public long yinshang_id { get; set; }

        [StringLength(200)]
        public string sale_order_code { get; set; }

        [StringLength(50)]
        public string id_card { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        [StringLength(11)]
        public string phone { get; set; }

        public decimal? money { get; set; }

        [StringLength(50)]
        public string pay_number { get; set; }

        public int? is_sync { get; set; }

        [StringLength(100)]
        public string dealer_shop_id { get; set; }

        public DateTime? create_time { get; set; }

        public int? pay_type { get; set; }
    }
}
