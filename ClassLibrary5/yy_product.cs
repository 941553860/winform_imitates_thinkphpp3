namespace ClassLibrary5
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yongyou.yy_product")]
    public partial class yy_product
    {
        [Key]
        [StringLength(36)]
        public string yy_product_id { get; set; }

        [StringLength(200)]
        public string yy_product_code { get; set; }

        [StringLength(200)]
        public string yy_product_name { get; set; }

        [StringLength(200)]
        public string yy_product_zh_name { get; set; }

        public decimal? yy_product_weight { get; set; }

        [Column(TypeName = "date")]
        public DateTime? produce_date { get; set; }

        [StringLength(200)]
        public string private_code { get; set; }

        [StringLength(50)]
        public string batch_number { get; set; }

        public int? quality_status { get; set; }

        [StringLength(36)]
        public string product_id { get; set; }

        [StringLength(36)]
        public string product_level_id { get; set; }

        [StringLength(36)]
        public string quality_detail_id { get; set; }

        [StringLength(200)]
        public string product_index { get; set; }

        public int? del_flag { get; set; }

        public int? quality_detail_status { get; set; }

        public int? print_flag { get; set; }
    }
}
